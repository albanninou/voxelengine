#pragma once
#include "Engine.h"
#include "CurrentTime.h"

vec2 Engine::mouseDelta = vec2(0, 0);
sf::Vector2i Engine::localPosition = sf::Vector2i(0, 0);

Engine::Engine()
{
	sf::ContextSettings settings;
	settings.depthBits = 24;
	settings.stencilBits = 8;
	settings.antialiasingLevel = 4;
	//settings.majorVersion = 3;
	//settings.minorVersion = 1;
	srand((unsigned int)time(NULL));
	window = new sf::Window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Default, settings);
	window->setVerticalSyncEnabled(false);

	// activate the window
	window->setActive(true);
	settings = window->getSettings();

	std::cout << "depth bits:" << settings.depthBits << std::endl;
	std::cout << "stencil bits:" << settings.stencilBits << std::endl;
	std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
	std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;
	glewExperimental = GL_TRUE;
	glewInit();
	glViewport(0, 0, 800, 600);
	createProjectionMatrix();
	glEnable(GL_DEPTH_TEST);
}

Engine::~Engine()
{
	vector<GameObject*>::iterator it;
	for (it = gameObjects.begin(); it != gameObjects.end(); it++)
	{
		delete(*it);
	}
	delete(window);
}

void Engine::Start()
{
	CurrentTime  now;
	uint64_t before = (uint64_t)now.microseconds();
	uint64_t before2 = (uint64_t)now.microseconds();
	uint64_t ns = (uint64_t)(1000000.0 / 60.0);
	uint64_t ns2 = (uint64_t)(1000000.0 / 99999.0);
	uint64_t timer = (uint64_t)now.microseconds();

	sf::Mouse::setPosition(sf::Vector2i(400, 300), *window);
	localPosition = sf::Mouse::getPosition(*window);
	mouseDelta = vec2(0, 0);
	window->setMouseCursorGrabbed(true);
	window->setMouseCursorVisible(false);

	int frames = 0;
	int ticks = 0;

	bool running = true;
	while (running)
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				running = false;
			}
			else if (event.type == sf::Event::Resized)
			{
				glViewport(0, 0, event.size.width, event.size.height);
				createProjectionMatrix();
				gameObjectRender.updateProjectionMatrix(projectionMatrix);
			}
			else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == 51) {
					running = false;
				}
			}
		}

		uint64_t now2 = (uint64_t)now.microseconds();
		uint64_t elapsed = now2 - before;
		uint64_t elapsed2 = now2 - before2;

		if (elapsed > ns) {
			update();
			ticks++;
			before += ns;
		}
		else {
			if (elapsed2 > ns2) {
				if (mouseGrabbed) {
					render();
					frames++;
					before2 += ns2;
					window->display();
				}
			}
		}

		if (now.microseconds() - timer > 1000000) {
			string title = to_string(frames) + " fps, " + to_string(ticks) + " ticks\0";
			window->setTitle(title.c_str());
			frames = 0;
			ticks = 0;
			timer = (uint64_t)now.microseconds();
		}
	}
}

void Engine::addGameObject(GameObject* gameObject)
{
	gameObjects.push_back(gameObject);
	gameObjectRender.addGameObject(gameObject);
}

void Engine::initShader(Shader* shader)
{
	shader->start();
	shader->loadProjectionMatrix(projectionMatrix);
	shader->stop();
}

void Engine::update()
{
	if (mouseGrabbed) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			mouseGrabbed = false;
			window->setMouseCursorGrabbed(false);
			window->setMouseCursorVisible(true);
			return;
		}
		sf::Vector2i tempo = sf::Mouse::getPosition(*window);
		mouseDelta.x = (float)tempo.x - Engine::localPosition.x;
		mouseDelta.y = (float)tempo.y - Engine::localPosition.y;
		Engine::localPosition = tempo;
		vector<GameObject*>::iterator it;
		for (it = gameObjects.begin(); it != gameObjects.end(); it++)
		{
			(*it)->update();
		}
		sf::Mouse::setPosition(sf::Vector2i(400, 300), *window);
		localPosition = sf::Mouse::getPosition(*window);
		mouseDelta = vec2(0, 0);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1)) {
			wireframe = !wireframe;
			if (wireframe) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}
			else {
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}
		}
	}
	else {
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && window->hasFocus()) {
			mouseGrabbed = true;
			window->setMouseCursorGrabbed(true);
			window->setMouseCursorVisible(false);
			sf::Mouse::setPosition(sf::Vector2i(400, 300), *window);
			localPosition = sf::Mouse::getPosition(*window);
			mouseDelta = vec2(0, 0);
		}
	}
}

void Engine::render()
{
	glClearColor(0.8f, 0.9f, 1.0f, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	gameObjectRender.render();
}

void Engine::createProjectionMatrix()
{
	int width = window->getSize().x;
	int height = window->getSize().y;
	float aspectRatio = (float)width / (float)height;
	float y_scale = (float)(1.0f / tan(radians((FOV / 2.0f))) * aspectRatio);
	float x_scale = y_scale / aspectRatio;
	float frustum_length = FAR_PLANE - NEAR_PLANE;

	projectionMatrix = mat4(0.0f);
	projectionMatrix[0][0] = x_scale;
	projectionMatrix[1][1] = y_scale;
	projectionMatrix[2][2] = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
	projectionMatrix[2][3] = -1;
	projectionMatrix[3][2] = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
	projectionMatrix[3][3] = 0;
}