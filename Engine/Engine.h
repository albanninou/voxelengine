#pragma once
#include <gl/glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

#include <iostream>
#include <string>
#include <chrono>
#include <vector>

#include "Tool\Tools.h"
#include "GameObject\Components\Shader.h"
#include "Tool\Maths.h"
#include "GameObject\Components\Camera.h"
#include "GameObjectRender.h"
#include "GameObject\Components\Voxel\VoxelsComponent.h"
#include "GameObject\Components\BoundingBox.h"
#include "GameObject\Cube.h"

class Engine {

private:
	vector<GameObject*> gameObjects;
	sf::Window* window;
	GameObjectRender gameObjectRender;
	float FOV = 70;
	float NEAR_PLANE = 0.1f;
	float FAR_PLANE = 1000;
	bool mouseGrabbed = true;
	mat4 projectionMatrix;
	bool wireframe = false;

public:
	static vec2 mouseDelta;
	static sf::Vector2i localPosition;

	Engine();
	~Engine();

	void Start();
	void addGameObject(GameObject* gameObject);
	void initShader(Shader* shader);

private:
	void update();
	void render();
	void createProjectionMatrix();
};