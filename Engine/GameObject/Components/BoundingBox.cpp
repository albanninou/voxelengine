#include "BoundingBox.h"

BoundingBox::BoundingBox() :haveVoxel(false)
{
}

BoundingBox::~BoundingBox()
{
}

void BoundingBox::update()
{

}

void BoundingBox::updateVoxel()
{
	VoxelsComponent* voxelComponent = gameObject->getComponent<VoxelsComponent>();
	if (voxelComponent != nullptr) {
		min = voxelComponent->getMin();
		max = voxelComponent->getMax();
	}
	else {
		min = vec3(-gameObject->getScale().x / 2.0, -gameObject->getScale().y / 2.0, -gameObject->getScale().x / 2.0);
		max = vec3(gameObject->getScale().x / 2.0, gameObject->getScale().y / 2.0, gameObject->getScale().x / 2.0);
	}
}

void BoundingBox::render()
{
	/*glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDisable(GL_CULL_FACE);
	glLineWidth(4);
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);

	glVertex3f(position.x*0.5f + max.x + 1, position.y *0.5f + max.y + 1, position.z*0.5f + min.z);
	glVertex3f(position.x*0.5f + min.x, position.y*0.5f + max.y + 1, position.z *0.5f + min.z);
	glVertex3f(position.x*0.5f + min.x, position.y*0.5f + max.y + 1, position.z *0.5f + max.z + 1);
	glVertex3f(position.x*0.5f + max.x + 1, position.y*0.5f + max.y + 1, position.z *0.5f + max.z + 1);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(position.x*0.5f + max.x + 1, position.y *0.5f + min.y, position.z *0.5f + max.z + 1);
	glVertex3f(position.x*0.5f + min.x, position.y *0.5f + min.y, position.z *0.5f + max.z + 1);
	glVertex3f(position.x*0.5f + min.x, position.y *0.5f + min.y, position.z *0.5f + min.z);
	glVertex3f(position.x*0.5f + max.x + 1, position.y*0.5f + min.y, position.z *0.5f + min.z);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(position.x*0.5f + max.x + 1, position.y *0.5f + min.y, position.z*0.5f + min.z);
	glVertex3f(position.x*0.5f + min.x, position.y *0.5f + min.y, position.z*0.5f + min.z);
	glVertex3f(position.x*0.5f + min.x, position.y *0.5f + max.y + 1, position.z *0.5f + min.z);
	glVertex3f(position.x*0.5f + max.x + 1, position.y*0.5f + max.y + 1, position.z *0.5f + min.z);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(position.x*0.5f + min.x, position.y *0.5f + max.y + 1, position.z *0.5f + max.z + 1);
	glVertex3f(position.x*0.5f + min.x, position.y *0.5f + max.y + 1, position.z*0.5f + min.z);
	glVertex3f(position.x *0.5f + min.x, position.y *0.5f + min.y, position.z*0.5f + min.z);
	glVertex3f(position.x*0.5f + min.x, position.y *0.5f + min.y, position.z*0.5f + max.z + 1);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(position.x *0.5f + max.x + 1, position.y*0.5f + max.y + 1, position.z*0.5f + min.z);
	glVertex3f(position.x*0.5f + max.x + 1, position.y *0.5f + max.y + 1, position.z *0.5f + max.z + 1);
	glVertex3f(position.x *0.5f + max.x + 1, position.y*0.5f + min.y, position.z *0.5f + max.z + 1);
	glVertex3f(position.x*0.5f + max.x + 1, position.y *0.5f + min.y, position.z *0.5f + min.z);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);*/

}

void BoundingBox::setGameObject(GameObject * gameObject)
{
	this->gameObject = gameObject;
	this->position = gameObject->getPosition();
}
