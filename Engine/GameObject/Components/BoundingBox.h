#pragma once
#include "Component.h"
#include "Voxel\VoxelsComponent.h"

class BoundingBox : public Component
{
public:
	BoundingBox();
	~BoundingBox();

	void update();
	void updateVoxel();
	void render();
	void setGameObject(GameObject* gameObject);

private:
	bool haveVoxel;
	ivec3 min;
	ivec3 max;

};
