#include "Camera.h"

Camera::Camera()
{
	viewMatrix = new mat4();
	needUpdateViewMatrix = true;
}

Camera::~Camera()
{
	delete viewMatrix;
}

void Camera::setGameObject(GameObject * gameObject)
{
	this->gameObject = gameObject;
	this->position = gameObject->getPosition();
	this->rotation = gameObject->getRotation();
	needUpdateViewMatrix = true;
}

void Camera::addRotation(vec3& rot)
{
	if (rot.x != 0 || rot.y != 0 || rot.z != 0) {
		rotation += rot;
		needUpdateViewMatrix = true;
	}
}

void Camera::addPosition(vec3& rot)
{
	if (rot.x != 0 || rot.y != 0 || rot.z != 0) {
		position += rot;
		needUpdateViewMatrix = true;
	}
}

vec3 Camera::getRotation() const
{
	return rotation;
}

vec3 Camera::getPosition() const
{
	return position;
}

mat4 * Camera::getViewMatrix()
{
	if (needUpdateViewMatrix) {
		*viewMatrix = createViewMatrix(position, rotation);
		needUpdateViewMatrix = false;
	}
	return viewMatrix;
}