#pragma once
#include "..\..\Tool\Tools.h"
#include "Component.h"
#include "..\GameObject.h"

class Camera : public Component
{
private:

	mat4* viewMatrix;
	bool needUpdateViewMatrix;

public:
	Camera();
	~Camera();
	void setGameObject(GameObject* gameObject);
	void addRotation(vec3& rot);
	void addPosition(vec3& pos);
	vec3 getRotation() const;
	vec3 getPosition() const;
	mat4* getViewMatrix();
private:

};

