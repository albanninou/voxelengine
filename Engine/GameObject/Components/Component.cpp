#pragma once
#include "..\..\Tool\Tools.h"
#include "Component.h"
#include "..\GameObject.h"

Component::Component()
{
}

Component::~Component()
{
}

void Component::render()
{
}

void Component::update()
{
}

void Component::updateVoxel()
{
}

void Component::setGameObject(GameObject* gameObject)
{
	(*this).gameObject = gameObject;
}

void Component::addRotation(vec3& rot)
{
	rotation += rot;
}

void Component::addPosition(vec3& rot)
{
	position += rot;
}

GameObject* Component::getGameObject() const
{
	return this->gameObject;
}