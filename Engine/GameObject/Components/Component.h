#pragma once
#include<iostream>
#include "..\..\Tool\Tools.h"


class GameObject;

class Component
{
public:
	vec3 position;
	vec3 rotation;
	Component();
	virtual ~Component();
	virtual void render();
	virtual void update();
	virtual void updateVoxel();
	virtual void setGameObject(GameObject* gameObject);
	virtual void addRotation(vec3& rot);
	virtual void addPosition(vec3& rot);
	virtual GameObject* getGameObject() const;
protected:
	GameObject* gameObject;
};