#include "Light.h"

Light::Light(vec3& color) :color(color), attenuation(vec3(1, 0, 0))
{

}

Light::Light(vec3& color, vec3& attenuation) : color(color), attenuation(attenuation)
{

}

Light::~Light()
{
}

void Light::setGameObject(GameObject * gameObject)
{
	this->gameObject = gameObject;
	this->position = gameObject->getPosition();
	this->rotation = gameObject->getRotation();
}