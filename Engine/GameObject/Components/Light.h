#pragma once
#include "Component.h"
#include "..\GameObject.h"

class Light :public Component
{
public:
	vec3 color;
	vec3 attenuation;

	Light(vec3& color);
	Light(vec3& color, vec3& attenuation);
	~Light();

	void setGameObject(GameObject* gameObject);

private:

};