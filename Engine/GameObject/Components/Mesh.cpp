#pragma once
#include "Mesh.h"

Mesh::Mesh(vector<float> vertices, vector<float> normals, vector<float> colour, vector<int> indices)
{
	verticesCount = vertices.size() / 3;
	indicesCount = indices.size();
	vaoID = createVAO();
	vboIndex = bindIndicesBuffer(indices);
	vboVertices = storeDataInAttributeList(0, vertices);
	vboColour = storeDataInAttributeList(1, colour);
	vboNormals = storeDataInAttributeList(2, normals);
	unbindVAO();
}

void Mesh::render()
{
	if (indicesCount == 0) return;
	glBindVertexArray(vaoID);
	glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Mesh::update()
{
}

void Mesh::updateMesh(vector<float> vertices, vector<float> normals, vector<float> colour, vector<int> indices)
{
	glBindVertexArray(vaoID);
	verticesCount = vertices.size() / 3;
	indicesCount = indices.size();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndex);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vboVertices);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vboColour);
	glBufferData(GL_ARRAY_BUFFER, colour.size() * sizeof(float), &colour[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	unbindVAO();
}

GLuint Mesh::createVAO()
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	return vaoID;
}

GLuint Mesh::bindIndicesBuffer(vector<int> indices) {
	GLuint vboID = 0;
	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), &indices[0], GL_STATIC_DRAW);
	return vboID;
}

GLuint Mesh::storeDataInAttributeList(int attributeNumber, vector<float> vertices)
{
	GLuint vboID = 0;
	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, 3, GL_FLOAT, false, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return vboID;
}

void Mesh::unbindVAO()
{
	glBindVertexArray(0);
}