#pragma once
#include <gl/glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

#include <iostream>

#include "Component.h"

class Mesh : public Component
{
private:
	GLuint vaoID;
	GLuint vboVertices;
	GLuint vboNormals;
	GLuint vboColour;
	GLuint vboIndex;
	int verticesCount;
	int indicesCount;

public:
	Mesh(vector<float> vertices, vector<float> normals, vector<float> colour, vector<int> indices);
	void render();
	void update();
	void updateMesh(vector<float> vertices, vector<float> normals, vector<float> colour, vector<int> indices);
private:
	GLuint createVAO();
	GLuint bindIndicesBuffer(vector<int> indices);
	GLuint storeDataInAttributeList(int attributeNumber, vector<float> vertices);
	void unbindVAO();
};