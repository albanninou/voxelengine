#pragma once
#include "Shader.h"

Shader::Shader(string vertexFile, string fragmentFile)
{
	vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
	fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	cout << "Shader charger" << endl;
	bindAttributes();
	glLinkProgram(programID);
	glValidateProgram(programID);
	cout << "Shader valider" << endl;
	getAllLocations();
}

Shader::~Shader()
{
	stop();
	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);
	glDeleteProgram(programID);
}

void Shader::bindAttributes()
{
	bindAttribute(0, "position");
	bindAttribute(1, "colour");
	bindAttribute(2, "normal");
	cout << "bind attribute" << endl;
}

void Shader::getAllLocations()
{
	location_transformationMatrix = getLocation("transformationMatrix");
	location_projectionMatrix = getLocation("projectionMatrix");
	location_viewMatrix = getLocation("viewMatrix");
	location_shineDamper = getLocation("shineDamper");
	location_reflectivity = getLocation("reflectivity");

	for (int i = 0; i < 5; i++) {
		char test[50];
		sprintf_s(test, "lightPosition[%d]", i);
		location_lightPosition[i] = getLocation(test);

		sprintf_s(test, "lightColour[%d]", i);
		location_lightColour[i] = getLocation(test);

		sprintf_s(test, "attenuation[%d]", i);
		location_lightAttenuation[i] = getLocation(test);
	}
	cout << "getAllLocation" << endl;
}

void Shader::start()
{
	glUseProgram(programID);
}

void Shader::stop()
{
	glUseProgram(0);
}

void Shader::bindAttribute(int attribute, string variableName)
{
	const char *c_str = variableName.c_str();
	glBindAttribLocation(programID, attribute, c_str);
}

int Shader::getLocation(string uniformName)
{
	const char *c_str = uniformName.c_str();
	return glGetUniformLocation(programID, c_str);
}

void Shader::loadFloat(int location, float value)
{
	glUniform1f(location, value);
}

void Shader::loadVector(int location, vec3& vec)
{
	glUniform3f(location, vec.x, vec.y, vec.z);
}

void Shader::loadBoolean(int location, bool value)
{
	float toLoad = 0;
	if (value) {
		toLoad = 1;
	}
	glUniform1f(location, toLoad);
}

void Shader::loadMatrix(int location, mat4& matrix)
{
	glUniformMatrix4fv(location, 1, false, glm::value_ptr(matrix));
}

void Shader::loadTransformationMatrix(mat4& matrix)
{
	loadMatrix(location_transformationMatrix, matrix);
}

void Shader::loadViewMatrix(mat4 & matrix)
{
	loadMatrix(location_viewMatrix, matrix);
}

void Shader::loadProjectionMatrix(mat4 & matrix)
{
	loadMatrix(location_projectionMatrix, matrix);
}

void Shader::loadLigths(vector<Light*>& lights)
{
	for (int i = 0; i < 5; i++) {
		if (i < (int)lights.size()) {
			if (lights[i] == nullptr) continue;
			loadVector(location_lightPosition[i], lights[i]->position);
			loadVector(location_lightColour[i], lights[i]->color);
			loadVector(location_lightAttenuation[i], lights[i]->attenuation);
		}
		else {
			loadVector(location_lightPosition[i], vec3(0, 0, 0));
			loadVector(location_lightColour[i], vec3(0, 0, 0));
			loadVector(location_lightAttenuation[i], vec3(1, 0, 0));

		}
	}
}

void Shader::loadShineVariable(float damper, float reflectivity)
{
	loadFloat(location_shineDamper, damper);
	loadFloat(location_reflectivity, reflectivity);
}

void Shader::render()
{
}

void Shader::update()
{
}

void Shader::setGameObject(GameObject * gameObject)
{
	(*this).gameObject = nullptr;
}

int Shader::loadShader(string file, int type) {
	string shaderSource;
	try {
		ifstream infile(file);
		string line;
		while (getline(infile, line)) {
			shaderSource.append(line).append("//\n");
		}
		infile.close();
	}
	catch (exception e) {
		cout << "Erreur lors du chargement du shader : " << file << endl;
	}
	int shaderID = glCreateShader(type);
	const char *c_str = shaderSource.c_str();
	glShaderSource(shaderID, 1, &c_str, NULL);
	glCompileShader(shaderID);
	GLint isCompile;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isCompile);
	if (isCompile == GL_FALSE) {
		cout << "Could not compile shader!" << endl;
	}

	return shaderID;
}