#pragma once
#include "Component.h"
#include "Light.h"
#include "..\..\Tool\Tools.h"

class Shader : public Component
{
private:
	int programID;
	int vertexShaderID;
	int fragmentShaderID;

	int location_transformationMatrix;
	int location_projectionMatrix;
	int location_viewMatrix;
	int location_lightPosition[5];
	int location_lightColour[5];
	int location_lightAttenuation[5];
	int location_shineDamper;
	int location_reflectivity;

public:
	Shader(string vertexFile, string fragmentFile);
	~Shader();
	void start();
	void stop();
	void loadTransformationMatrix(mat4& matrix);
	void loadViewMatrix(mat4& matrix);
	void loadProjectionMatrix(mat4& matrix);
	void loadLigths(vector<Light*>& lights);
	void loadShineVariable(float damper, float reflectivity);
	void render();
	void update();
	void setGameObject(GameObject* gameObject);
private:
	int loadShader(string file, int type);
	void bindAttributes();
	void bindAttribute(int attribute, string variableName);
	int getLocation(string uniformName);
	void getAllLocations();
	void loadFloat(int location, float value);
	void loadVector(int location, vec3& vec);
	void loadBoolean(int location, bool value);
	void loadMatrix(int location, mat4& matrix);
};