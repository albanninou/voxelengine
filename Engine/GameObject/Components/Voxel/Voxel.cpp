#pragma once
#include "Voxel.h"

Voxel::Voxel(ivec3 pos, ivec3 size,int type, vec3 color)
{
	this->type = type;
	this->color = color;
	this->pos = pos;
	this->size = size;
}

Voxel::Voxel(Voxel const & voxel)
{
	this->type = voxel.type;
	this->color = voxel.color;
}

Voxel::~Voxel()
{
}

void addVoxelDataUp(vector<float>& vertices, float x, float y, float z, int size, int size2, int size3)
{
	float tempo[] = { //
			x, y + size2, z, //
			x + size, y + size2, z, //
			x + size, y + size2, z + size3, //
			x, y + 1, z + size3, //
	};
	vertices.insert(vertices.end(), std::begin(tempo), std::end(tempo));
}

void addVoxelDataDown(vector<float>& vertices, float x, float y, float z, int size, int size2, int size3)
{
	float tempo[] = { //
		x + size, y, z, //
		x, y, z, //
		x, y, z + size3, //
		x + size, y, z + size3, //
	};
	vertices.insert(vertices.end(), std::begin(tempo), std::end(tempo));
}

void addVoxelDataLeft(vector<float>& vertices, float x, float y, float z, int size, int size2, int size3)
{
	float tempo[] = { //
		x, y, z, //
		x, y + size2, z, //
		x, y + size2, z + size3, //
		x, y, z + size3, //
	};
	vertices.insert(vertices.end(), std::begin(tempo), std::end(tempo));
}

void addVoxelDataRight(vector<float>& vertices, float x, float y, float z, int size, int size2, int size3)
{
	float tempo[] = { //
		x + size, y + size2, z, //
		x + size, y, z, //
		x + size, y, z + size3, //
		x + size, y + size2, z + size3, //
	};
	vertices.insert(vertices.end(), std::begin(tempo), std::end(tempo));
}

void addVoxelDataFront(vector<float>& vertices, float x, float y, float z, int size, int size2, int size3)
{
	float tempo[] = { //
		x, y, z, //
		x + size, y, z, //
		x + size, y + size2, z, //
		x, y + size2, z, //
	};
	vertices.insert(vertices.end(), std::begin(tempo), std::end(tempo));
}

void addVoxelDataBack(vector<float>& vertices, float x, float y, float z, int size, int size2, int size3)
{
	float tempo[] = { //
		x + size, y, z + size3, //
		x, y, z + size3, //
		x, y + size2, z + size3, //
		x + size, y + size2, z + size3, //
	};
	vertices.insert(vertices.end(), std::begin(tempo), std::end(tempo));
}

void addIndiceData(vector<int>& indices)
{
	int n = indices.size() / 6 * 4;
	vector<int> tempo = { //
		0 + n,  3 + n,1 + n, // top left triangle (v0, v1, v3)
		3 + n, 2 + n,1 + n,   // bottom right triangle (v3, v1, v2)
	};
	indices.insert(indices.end(), std::begin(tempo), std::end(tempo));
}

void Voxel::addColorData(vector<float>& colors)
{
	float tempo[] = { //
		color.x,color.y,color.z,//
		color.x,color.y,color.z,//
		color.x,color.y,color.z,//
		color.x,color.y,color.z,//
	};
	colors.insert(colors.end(), std::begin(tempo), std::end(tempo));
}

int Voxel::getType() const
{
	return type;
}

void addNormalDataUp(vector<float>& normals)
{
	float tempo[] = { //
		0.0f, 1.0f, 0.0f, //
		0.0f, 1.0f, 0.0f, //
		0.0f, 1.0f, 0.0f,//
		0.0f, 1.0f, 0.0f
	};

	normals.insert(normals.end(), std::begin(tempo), std::end(tempo));
}

void addNormalDataDown(vector<float>& normals)
{
	float tempo[] = { //
		0.0f, -1.0f, 0.0f, //
		0.0f, -1.0f, 0.0f, //
		0.0f, -1.0f, 0.0f,//
		0.0f, -1.0f, 0.0f
	};
	normals.insert(normals.end(), std::begin(tempo), std::end(tempo));
}

void addNormalDataLeft(vector<float>& normals)
{
	float tempo[] = { //
		-1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f
	};
	normals.insert(normals.end(), std::begin(tempo), std::end(tempo));
}

void addNormalDataRight(vector<float>& normals)
{
	float tempo[] = { //
		1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f
	};
	normals.insert(normals.end(), std::begin(tempo), std::end(tempo));
}

void addNormalDataFront(vector<float>& normals)
{
	float tempo[] = { //
		0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f
	};
	normals.insert(normals.end(), std::begin(tempo), std::end(tempo));
}

void addNormalDataBack(vector<float>& normals)
{
	float tempo[] = { //
		0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
	};

	normals.insert(normals.end(), std::begin(tempo), std::end(tempo));
}

bool operator<(const Voxel & l, const Voxel & r)
{
	long hash = 17;
	hash = hash * 10000 + (int)l.pos.x & 0x00000000ffffffffL;
	hash = hash * 10000 + (int)l.pos.y & 0x00000000ffffffffL;
	hash = hash * 10000 + (int)l.pos.z & 0x00000000ffffffffL;

	long hash2 = 17;
	hash2 = hash2 * 10000 + (int)r.pos.x & 0x00000000ffffffffL;
	hash2 = hash2 * 10000 + (int)r.pos.y & 0x00000000ffffffffL;
	hash2 = hash2 * 10000 + (int)r.pos.z & 0x00000000ffffffffL;

	return hash < hash2;
}

bool operator==(const Voxel & l, const Voxel & r)
{
	return l.pos.x == r.pos.x && l.pos.y == r.pos.y && l.pos.z == r.pos.z && l.size.x == r.size.x && l.size.y == r.size.y && l.size.z == r.size.z;
}
