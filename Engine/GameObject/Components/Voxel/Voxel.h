#pragma once
#include "..\..\..\Tool\Tools.h"

class Voxel
{
public:
	Voxel(ivec3 pos, ivec3 size, int type, vec3 color);
	Voxel(Voxel const& voxel);
	~Voxel();

	void addColorData(vector<float> &colors);
	int getType() const;

	vec3 color;
	ivec3 pos;
	ivec3 size;

private:

	int type;

};

void addVoxelDataUp(vector<float> &vertices, float x, float y, float z, int size, int size2, int size3);
void addVoxelDataDown(vector<float> &vertices, float x, float y, float z, int size, int size2, int size3);
void addVoxelDataLeft(vector<float> &vertices, float x, float y, float z, int size, int size2, int size3);
void addVoxelDataRight(vector<float> &vertices, float x, float y, float z, int size, int size2, int size3);
void addVoxelDataFront(vector<float> &vertices, float x, float y, float z, int size, int size2, int size3);
void addVoxelDataBack(vector<float> &vertices, float x, float y, float z, int size, int size2, int size3);
void addIndiceData(vector<int> &indices);
void addNormalDataUp(vector<float> &normals);
void addNormalDataDown(vector<float> &normals);
void addNormalDataLeft(vector<float> &normals);
void addNormalDataRight(vector<float> &normals);
void addNormalDataFront(vector<float> &normals);
void addNormalDataBack(vector<float> &normals);

bool operator <(const Voxel& l, const Voxel& r);
bool operator ==(const Voxel& l, const Voxel& r);