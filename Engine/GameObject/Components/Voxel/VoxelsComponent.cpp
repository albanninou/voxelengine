#include "VoxelsComponent.h"

VoxelsComponent::VoxelsComponent()
{
	needUpdateMesh = true;
	min = ivec3(0, 0, 0);
	max = ivec3(0, 0, 0);
}

VoxelsComponent::~VoxelsComponent()
{
	vector<Voxel*>::iterator it;
	for (it = allVoxels.begin(); it != allVoxels.end(); it++)
	{
		delete (*it);
	}
}


void VoxelsComponent::setVoxel(Voxel* voxel)
{
	ivec3 vec = voxel->pos;
	ivec3 size = voxel->size - ivec3(1, 1, 1);

	if (vec.x < min.x) {
		min.x = vec.x;
	}
	if (vec.x + size.x > max.x) {
		max.x = vec.x + size.x;
	}
	if (vec.y < min.y) {
		min.y = vec.y;
	}
	if (vec.y + size.y > max.y) {
		max.y = vec.y + size.y;
	}
	if (vec.z < min.z) {
		min.z = vec.z;
	}
	if (vec.z + size.z > max.z) {
		max.z = vec.z + size.z;
	}

	for (int x = vec.x; x <= vec.x + size.x; x++) {
		for (int y = vec.y; y <= vec.y + size.y; y++) {
			for (int z = vec.z; z <= vec.z + size.z; z++) {
				ivec3 pos = ivec3(x, y, z);
				Voxel* find = getVoxelAt(pos);
				if (find != nullptr) {
					if (find->getType() != voxel->getType()) {
						splitVoxelAt(pos);
					}
				}
				else {
					voxels[ivec3(x, y, z)] = voxel;

				}
			}
		}
	}

	needUpdateMesh = true;
	allVoxels.push_back(voxel);
}

Voxel * VoxelsComponent::getVoxelAt(ivec3 vec)
{
	return voxels[vec];
}

Voxel * VoxelsComponent::getVoxelAt(int x, int y, int z)
{
	return getVoxelAt(ivec3(x, y, z));
}

void VoxelsComponent::splitVoxelAt(ivec3 vec)
{
	Voxel* find = getVoxelAt(vec);
	if (find == nullptr) return;

	if (vec.y > find->pos.y) {
		int sizeY = vec.y - find->pos.y;
		Voxel* newVoxel = new Voxel(find->pos, ivec3(find->size.x, sizeY, find->size.z), find->getType(), find->color);
		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < newVoxel->size.x; x++) {
				for (int z = 0; z < newVoxel->size.z; z++) {
					voxels[ivec3(newVoxel->pos.x + x, newVoxel->pos.y + y, newVoxel->pos.z + z)] = newVoxel;
				}
			}
		}
		allVoxels.push_back(newVoxel);
		find->size.y -= sizeY;
		find->pos.y += sizeY;
	}

	if (vec.y < find->pos.y + find->size.y - 1) {
		int sizeY = find->pos.y + find->size.y - vec.y - 1;
		Voxel* newVoxel = new Voxel(find->pos, ivec3(find->size.x, sizeY, find->size.z), find->getType(), find->color);
		newVoxel->pos.y += find->size.y - sizeY;
		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < newVoxel->size.x; x++) {
				for (int z = 0; z < newVoxel->size.z; z++) {
					voxels[ivec3(newVoxel->pos.x + x, newVoxel->pos.y + y, newVoxel->pos.z + z)] = newVoxel;
				}
			}
		}
		allVoxels.push_back(newVoxel);
		find->size.y -= sizeY;
	}

	if (vec.x > find->pos.x) {
		int sizeX = vec.x - find->pos.x;
		Voxel* newVoxel = new Voxel(find->pos, ivec3(sizeX, 1, find->size.z), find->getType(), find->color);
		for (int x = 0; x < sizeX; x++) {
			for (int z = 0; z < newVoxel->size.z; z++) {
				voxels[ivec3(newVoxel->pos.x + x, newVoxel->pos.y, newVoxel->pos.z + z)] = newVoxel;
			}
		}
		allVoxels.push_back(newVoxel);
		find->size.x -= sizeX;
		find->pos.x += sizeX;
	}
	if (vec.x < find->pos.x + find->size.x - 1) {
		int sizeX = find->size.x - 1;
		Voxel* newVoxel = new Voxel(find->pos, ivec3(sizeX, 1, find->size.z), find->getType(), find->color);
		newVoxel->pos.x++;
		for (int x = 0; x < sizeX; x++) {
			for (int z = 0; z < newVoxel->size.z; z++) {
				voxels[ivec3(newVoxel->pos.x + x, newVoxel->pos.y, newVoxel->pos.z + z)] = newVoxel;
			}
		}
		allVoxels.push_back(newVoxel);
		find->size.x -= sizeX;
	}
	if (vec.z > find->pos.z) {
		int sizeZ = vec.z - find->pos.z;
		Voxel* newVoxel = new Voxel(find->pos, ivec3(1, 1, sizeZ), find->getType(), find->color);
		for (int z = 0; z < sizeZ; z++) {
			voxels[ivec3(newVoxel->pos.x, newVoxel->pos.y, newVoxel->pos.z + z)] = newVoxel;
		}
		allVoxels.push_back(newVoxel);
		find->size.z -= sizeZ;
		find->pos.z += sizeZ;
	}
	if (vec.z < find->pos.z + find->size.z - 1) {
		int sizeZ = find->size.z - 1;
		Voxel* newVoxel = new Voxel(find->pos, ivec3(1, 1, sizeZ), find->getType(), find->color);
		newVoxel->pos.z++;
		for (int z = 0; z < sizeZ; z++) {
			voxels[ivec3(newVoxel->pos.x, newVoxel->pos.y, newVoxel->pos.z + z)] = newVoxel;
		}
		allVoxels.push_back(newVoxel);
		find->size.z -= sizeZ;
	}

	if (find->size.x == 1 && find->size.y == 1 && find->size.z == 1) {
		cout << "Split succesfull" << endl;
		voxels[ivec3(find->pos.x, find->pos.y, find->pos.z)] = nullptr;
		allVoxels.erase(std::remove(allVoxels.begin(), allVoxels.end(), find), allVoxels.end());
		delete(find);
	}
	else {
		cout << "Split error" << endl;
	}
}

ivec3 VoxelsComponent::getMin() const
{
	return min;
}

ivec3 VoxelsComponent::getMax() const
{
	return max;
}

void VoxelsComponent::update()
{
	if (needUpdateMesh) {
		updateMesh();
		needUpdateMesh = false;
	}
}

void VoxelsComponent::updateMesh()
{
	vector<float> vertices;
	vector<float> normals;
	vector<float> colors;
	vector<int> indices;

	cout << "before reduce : " << allVoxels.size() << endl;
	reduceVoxel();
	cout << "after reduce : " << allVoxels.size() << endl;

	reduceMeshUp(vertices, normals, colors, indices);
	reduceMeshDown(vertices, normals, colors, indices);
	reduceMeshLeft(vertices, normals, colors, indices);
	reduceMeshRight(vertices, normals, colors, indices);
	reduceMeshFront(vertices, normals, colors, indices);
	reduceMeshBack(vertices, normals, colors, indices);

	gameObject->setCenter(vec3(min) + vec3(max.x / 2.0 + 0.5, max.y / 2.0 + 0.5, max.z / 2.0 + 0.5));
	gameObject->updateVoxel();
	if (vertices.size() == 0) return;
	Mesh* mesh = gameObject->getComponent<Mesh>();
	if (mesh == nullptr) {
		mesh = new Mesh(vertices, normals, colors, indices);
		gameObject->addComponent(mesh);
	}
	else {
		mesh->updateMesh(vertices, normals, colors, indices);
	}
}

void VoxelsComponent::reduceVoxel()
{
	bool change;
	do {
		change = false;

		for (int y = min.y; y <= max.y; y++) {
			for (int z = min.z; z <= max.z; z++) {
				for (int x = min.x; x < max.x; x++) {
					Voxel* actual = getVoxelAt(ivec3(x, y, z));
					if (actual != nullptr) {
						if (actual->pos.x + actual->size.x < max.x) {
							bool t;
							do {
								t = false;
								Voxel* next = getVoxelAt(ivec3(actual->pos.x + actual->size.x, y, z));
								if (next != nullptr) {
									if (actual->getType() == next->getType()) {
										if (actual->size.y == next->size.y && actual->size.z == next->size.z) {
											for (int i = next->pos.x; i < next->pos.x + next->size.x; i++) {
												for (int j = next->pos.y; j < next->pos.y + next->size.y; j++) {
													for (int k = next->pos.z; k < next->pos.z + next->size.z; k++) {
														voxels[ivec3(i, j, k)] = actual;
													}
												}
											}
											t = true;
											change = true;
											actual->size.x += next->size.x;
											allVoxels.erase(std::remove(allVoxels.begin(), allVoxels.end(), next), allVoxels.end());
											delete(next);
										}
									}
								}
							} while (t);
							x = actual->pos.x + actual->size.x - 1;
							if (actual->pos.x + actual->size.x - 1 == min.x + max.x) {
								z += actual->size.z - 1;
								if (actual->pos.z + actual->size.z - 1 == min.z + max.z) {
									y += actual->size.y - 1;
								}
							}
						}
					}
				}
			}
		}

		for (int y = min.y; y <= max.y; y++) {
			for (int x = min.x; x <= max.x; x++) {
				for (int z = min.z; z < max.z; z++) {
					Voxel* actual = getVoxelAt(ivec3(x, y, z));
					if (actual != nullptr) {
						if (actual->pos.z + actual->size.z < max.z) {

							bool t;
							do {
								t = false;
								Voxel* next = getVoxelAt(ivec3(x, y, actual->pos.z + actual->size.z));
								if (next != nullptr) {
									if (actual->getType() == next->getType()) {
										if (actual->size.y == next->size.y && actual->size.x == next->size.x) {
											for (int i = next->pos.x; i < next->pos.x + next->size.x; i++) {
												for (int j = next->pos.y; j < next->pos.y + next->size.y; j++) {
													for (int k = next->pos.z; k < next->pos.z + next->size.z; k++) {
														voxels[ivec3(i, j, k)] = actual;
													}
												}
											}
											t = true;
											change = true;
											actual->size.z += next->size.z;
											allVoxels.erase(std::remove(allVoxels.begin(), allVoxels.end(), next), allVoxels.end());
											delete(next);
										}
									}
								}
							} while (t);
							z = actual->pos.z + actual->size.z - 1;
							if (actual->pos.z + actual->size.z - 1 == min.z + max.z) {
								x += actual->size.x - 1;
								if (actual->pos.x + actual->size.x - 1 == min.x + max.x) {
									y += actual->size.y - 1;
								}
							}
						}
					}
				}
			}
		}

		for (int x = min.x; x <= max.x; x++) {
			for (int z = min.z; z <= max.z; z++) {
				for (int y = min.y; y < max.y; y++) {
					Voxel* actual = getVoxelAt(ivec3(x, y, z));
					if (actual != nullptr) {
						if (actual->pos.y + actual->size.y < max.y) {

							bool t;
							do {
								t = false;
								Voxel* next = getVoxelAt(ivec3(x, actual->pos.y + actual->size.y, z));
								if (next != nullptr) {
									if (actual->getType() == next->getType()) {
										if (actual->size.z == next->size.z && actual->size.x == next->size.x) {
											for (int i = next->pos.x; i < next->pos.x + next->size.x; i++) {
												for (int j = next->pos.y; j < next->pos.y + next->size.y; j++) {
													for (int k = next->pos.z; k < next->pos.z + next->size.z; k++) {
														voxels[ivec3(i, j, k)] = actual;
													}
												}
											}
											t = true;
											change = true;
											actual->size.y += next->size.y;
											allVoxels.erase(std::remove(allVoxels.begin(), allVoxels.end(), next), allVoxels.end());
											delete(next);
										}
									}
								}
							} while (t);
							y = actual->pos.y + actual->size.y - 1;
							if (actual->pos.y + actual->size.y - 1 == min.y + max.y) {
								z += actual->size.z - 1;
								if (actual->pos.z + actual->size.z - 1 == min.z + max.z) {
									x += actual->size.x - 1;
								}
							}
						}
					}
				}
			}
		}

	} while (change);
}

void VoxelsComponent::reduceMeshUp(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices)
{
	map<ivec3, bool> memo;
	for (int y = max.y; y >= min.y; y--) {
		int x = min.x, z = min.z;
		do {
			int xSize = 1, zSize = 1;
			Voxel* actualVoxel = getVoxelAt(x, y, z);
			if (actualVoxel != nullptr && getVoxelAt(x, y + 1, z) == nullptr) {
				if (memo.find(ivec3(x, y, z)) == memo.end()) {
					int actualType = actualVoxel->getType();
					xSize = actualVoxel->size.x - x + actualVoxel->pos.x;
					bool t = false;
					do {
						t = false;
						Voxel* nextVoxel = getVoxelAt(x + xSize, y, z);
						if (nextVoxel != nullptr && getVoxelAt(x + xSize, y + 1, z) == nullptr) {
							if (actualType == nextVoxel->getType() && memo.find(ivec3(x + xSize, y, z)) == memo.end()) {
								xSize += nextVoxel->size.x;
								t = true;
							}
						}
					} while (t);
					t = true;
					do {
						for (int i = 0; i < xSize; i++) {
							Voxel* nextVoxel = getVoxelAt(x + i, y, z + zSize);
							if (nextVoxel != nullptr && getVoxelAt(x + i, y + 1, z + zSize) == nullptr) {
								if (actualType != nextVoxel->getType() && memo.find(ivec3(x + i, y, z + zSize)) == memo.end()) {
									t = false;
								}
								else {
									i += nextVoxel->size.x - 1;
								}
							}
							else {
								t = false;
							}
						}
						if (t) {
							zSize++;
						}
					} while (t);

					addVoxelDataUp(vertices, (float)x, (float)y, (float)z, xSize, 1, zSize);
					addNormalDataUp(normals);
					addIndiceData(indices);
					actualVoxel->addColorData(colors);
					for (int a = 0; a < xSize; a++) {
						for (int b = 0; b < zSize; b++) {
							memo[ivec3(x + a, y, z + b)] = true;
						}
					}
				}
			}
			x += xSize;
			if (x == max.x + 1) {
				x = min.x;
				z++;
			}
		} while (z < max.z + 1);
	}
}

void VoxelsComponent::reduceMeshDown(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices)
{
	map<ivec3, bool> memo;
	for (int y = min.y; y <= max.y; y++) {
		int x = min.x, z = min.z;
		do {
			int xSize = 1, zSize = 1;
			Voxel* actualVoxel = getVoxelAt(x, y, z);
			if (actualVoxel != nullptr && getVoxelAt(x, y - 1, z) == nullptr) {
				if (memo.find(ivec3(x, y, z)) == memo.end()) {
					int actualType = actualVoxel->getType();
					xSize = actualVoxel->size.x - x + actualVoxel->pos.x;
					bool t = false;
					do {
						t = false;
						Voxel* nextVoxel = getVoxelAt(x + xSize, y, z);
						if (nextVoxel != nullptr && getVoxelAt(x + xSize, y - 1, z) == nullptr) {
							if (actualType == nextVoxel->getType() && memo.find(ivec3(x + xSize, y, z)) == memo.end()) {
								xSize += nextVoxel->size.x;
								t = true;
							}
						}
					} while (t);
					t = true;
					do {
						for (int i = 0; i < xSize; i++) {
							Voxel* nextVoxel = getVoxelAt(x + i, y, z + zSize);
							if (nextVoxel != nullptr && getVoxelAt(x + i, y - 1, z + zSize) == nullptr) {
								if (actualType != nextVoxel->getType() && memo.find(ivec3(x + i, y, z + zSize)) == memo.end()) {
									t = false;
								}
								else {
									i += nextVoxel->size.x - 1;
								}
							}
							else {
								t = false;
							}
						}
						if (t) {
							zSize++;
						}
					} while (t);

					addVoxelDataDown(vertices, (float)x, (float)y, (float)z, xSize, 1, zSize);
					addNormalDataDown(normals);
					addIndiceData(indices);
					actualVoxel->addColorData(colors);
					for (int a = 0; a < xSize; a++) {
						for (int b = 0; b < zSize; b++) {
							memo[ivec3(x + a, y, z + b)] = true;
						}
					}
				}
			}
			x += xSize;
			if (x == max.x + 1) {
				x = min.x;
				z++;
			}
		} while (z < max.z + 1);
	}
}

void VoxelsComponent::reduceMeshLeft(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices)
{
	map<ivec3, bool> memo;
	for (int x = min.x; x <= max.x; x++) {
		int y = min.y, z = min.z;
		do {
			int ySize = 1, zSize = 1;
			Voxel* actualVoxel = getVoxelAt(x, y, z);
			if (actualVoxel != nullptr && getVoxelAt(x - 1, y, z) == nullptr) {
				if (memo.find(ivec3(x, y, z)) == memo.end()) {
					int actualType = actualVoxel->getType();
					ySize = actualVoxel->size.y - y + actualVoxel->pos.y;
					bool t = false;
					do {
						t = false;
						Voxel* nextVoxel = getVoxelAt(x, y + ySize, z);
						if (nextVoxel != nullptr && getVoxelAt(x - 1, y + ySize, z) == nullptr) {
							if (actualType == nextVoxel->getType() && memo.find(ivec3(x, y + ySize, z)) == memo.end()) {
								ySize += nextVoxel->size.y;
								t = true;
							}
						}
					} while (t);
					t = true;
					do {
						for (int i = 0; i < ySize; i++) {
							Voxel* nextVoxel = getVoxelAt(x, y + i, z + zSize);
							if (nextVoxel != nullptr && getVoxelAt(x - 1, y + i, z + zSize) == nullptr) {
								if (actualType != nextVoxel->getType() && memo.find(ivec3(x, y + i, z + zSize)) == memo.end()) {
									t = false;
								}
								else {
									i += nextVoxel->size.y - 1;
								}
							}
							else {
								t = false;
							}
						}
						if (t) {
							zSize++;
						}
					} while (t);

					addVoxelDataLeft(vertices, (float)x, (float)y, (float)z, 1, ySize, zSize);
					addNormalDataLeft(normals);
					addIndiceData(indices);
					actualVoxel->addColorData(colors);

					for (int a = 0; a < ySize; a++) {
						for (int b = 0; b < zSize; b++) {
							memo[ivec3(x, y + a, z + b)] = true;
						}
					}
				}
			}
			y += ySize;
			if (y == max.y + 1) {
				y = min.y;
				z++;
			}
		} while (z < max.z + 1);
	}
}

void VoxelsComponent::reduceMeshRight(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices)
{
	map<ivec3, bool> memo;
	for (int x = max.x; x >= min.x; x--) {
		int y = min.y, z = min.z;
		do {
			int ySize = 1, zSize = 1;
			Voxel* actualVoxel = getVoxelAt(x, y, z);
			if (actualVoxel != nullptr && getVoxelAt(x + 1, y, z) == nullptr) {
				if (memo.find(ivec3(x, y, z)) == memo.end()) {
					int actualType = actualVoxel->getType();
					ySize = actualVoxel->size.y - y + actualVoxel->pos.y;
					bool t = false;
					do {
						t = false;
						Voxel* nextVoxel = getVoxelAt(x, y + ySize, z);
						if (nextVoxel != nullptr && getVoxelAt(x + 1, y + ySize, z) == nullptr) {
							if (actualType == nextVoxel->getType() && memo.find(ivec3(x, y + ySize, z)) == memo.end()) {
								ySize += nextVoxel->size.y;
								t = true;
							}
						}
					} while (t);
					t = true;
					do {
						for (int i = 0; i < ySize; i++) {
							Voxel* nextVoxel = getVoxelAt(x, y + i, z + zSize);
							if (nextVoxel != nullptr && getVoxelAt(x + 1, y + i, z + zSize) == nullptr) {
								if (actualType != nextVoxel->getType() && memo.find(ivec3(x, y + i, z + zSize)) == memo.end()) {
									t = false;
								}
								else {
									i += nextVoxel->size.y - 1;
								}
							}
							else {
								t = false;
							}
						}
						if (t) {
							zSize++;
						}
					} while (t);

					addVoxelDataRight(vertices, (float)x, (float)y, (float)z, 1, ySize, zSize);
					addNormalDataRight(normals);
					addIndiceData(indices);
					actualVoxel->addColorData(colors);

					for (int a = 0; a < ySize; a++) {
						for (int b = 0; b < zSize; b++) {
							memo[ivec3(x, y + a, z + b)] = true;
						}
					}
				}
			}
			y += ySize;
			if (y == max.y + 1) {
				y = min.y;
				z++;
			}

		} while (z < max.z + 1);
	}
}

void VoxelsComponent::reduceMeshFront(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices)
{
	map<ivec3, bool> memo;
	for (int z = min.z; z <= max.z; z++) {
		int x = min.x, y = min.y;
		do {
			int xSize = 1, ySize = 1;
			Voxel* actualVoxel = getVoxelAt(x, y, z);
			if (actualVoxel != nullptr && getVoxelAt(x, y, z - 1) == nullptr) {
				if (memo.find(ivec3(x, y, z)) == memo.end()) {
					int actualType = actualVoxel->getType();
					xSize = actualVoxel->size.x - x + actualVoxel->pos.x;
					bool t = false;
					do {
						t = false;
						Voxel* nextVoxel = getVoxelAt(x + xSize, y, z);
						if (nextVoxel != nullptr && getVoxelAt(x + xSize, y, z - 1) == nullptr) {
							if (actualType == nextVoxel->getType() && memo.find(ivec3(x + xSize, y, z)) == memo.end()) {
								xSize += nextVoxel->size.x;
								t = true;
							}
						}
					} while (t);
					t = true;
					do {
						for (int i = 0; i < xSize; i++) {
							Voxel* nextVoxel = getVoxelAt(x + i, y + ySize, z);
							if (nextVoxel != nullptr && getVoxelAt(x + i, y + ySize, z - 1) == nullptr) {
								if (actualType != nextVoxel->getType() && memo.find(ivec3(x + i, y + ySize, z)) == memo.end()) {
									t = false;
								}
								else {
									i += nextVoxel->size.x - 1;
								}
							}
							else {
								t = false;
							}
						}
						if (t) {
							ySize++;
						}
					} while (t);

					addVoxelDataFront(vertices, (float)x, (float)y, (float)z, xSize, ySize, 1);
					addNormalDataFront(normals);
					addIndiceData(indices);
					actualVoxel->addColorData(colors);

					for (int a = 0; a < xSize; a++) {
						for (int b = 0; b < ySize; b++) {
							memo[ivec3(x + a, y + b, z)] = true;
						}
					}
				}
			}
			x += xSize;
			if (x == max.x + 1) {
				x = min.x;
				y++;
			}

		} while (y < max.y + 1);
	}
}

void VoxelsComponent::reduceMeshBack(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices)
{
	map<ivec3, bool> memo;
	for (int z = max.z; z >= min.z; z--) {
		int x = min.x, y = min.y;
		do {
			int xSize = 1, ySize = 1;
			Voxel* actualVoxel = getVoxelAt(x, y, z);
			if (actualVoxel != nullptr && getVoxelAt(x, y, z + 1) == nullptr) {
				if (memo.find(ivec3(x, y, z)) == memo.end()) {
					int actualType = actualVoxel->getType();
					xSize = actualVoxel->size.x - x + actualVoxel->pos.x;
					bool t = false;
					do {
						t = false;
						Voxel* nextVoxel = getVoxelAt(x + xSize, y, z);
						if (nextVoxel != nullptr && getVoxelAt(x + xSize, y, z + 1) == nullptr) {
							if (actualType == nextVoxel->getType() && memo.find(ivec3(x + xSize, y, z)) == memo.end()) {
								xSize += nextVoxel->size.x;
								t = true;
							}
						}
					} while (t);
					t = true;
					do {
						for (int i = 0; i < xSize; i++) {
							Voxel* nextVoxel = getVoxelAt(x + i, y + ySize, z);
							if (nextVoxel != nullptr && getVoxelAt(x + i, y + ySize, z + 1) == nullptr) {
								if (actualType != nextVoxel->getType() && memo.find(ivec3(x + i, y + ySize, z)) == memo.end()) {
									t = false;
								}
								else {
									i += nextVoxel->size.x - 1;
								}
							}
							else {
								t = false;
							}
						}
						if (t) {
							ySize++;
						}
					} while (t);

					addVoxelDataBack(vertices, (float)x, (float)y, (float)z, xSize, ySize, 1);
					addNormalDataBack(normals);
					addIndiceData(indices);
					actualVoxel->addColorData(colors);

					for (int a = 0; a < xSize; a++) {
						for (int b = 0; b < ySize; b++) {
							memo[ivec3(x + a, y + b, z)] = true;
						}
					}
				}
			}
			x += xSize;
			if (x == max.x + 1) {
				x = min.x;
				y++;
			}
		} while (y < max.y + 1);
	}
}

bool glm::operator<(const glm::vec3 & l, const glm::vec3 & r)
{
	long hash = 17;
	hash = hash * 600000 + (int)l.x & 0x00000000ffffffffL;
	hash = hash * 600000 + (int)l.y & 0x00000000ffffffffL;
	hash = hash * 600000 + (int)l.z & 0x00000000ffffffffL;

	long hash2 = 17;
	hash2 = hash2 * 600000 + (int)r.x & 0x00000000ffffffffL;
	hash2 = hash2 * 600000 + (int)r.y & 0x00000000ffffffffL;
	hash2 = hash2 * 600000 + (int)r.z & 0x00000000ffffffffL;

	return hash < hash2;
}

bool glm::operator==(const glm::vec3 & l, const glm::vec3 & r)
{
	return l.x == r.x && l.y == r.y && l.z == r.z;
}