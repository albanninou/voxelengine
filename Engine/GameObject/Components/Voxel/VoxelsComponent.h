#pragma once
#include "..\Component.h"
#include "..\..\GameObject.h"
#include "..\..\..\Tool\Tools.h"
#include "Voxel.h"
#include <iostream>
#include <vector>

namespace glm {
	bool operator <(const glm::vec3& l, const glm::vec3& r);
	bool operator ==(const glm::vec3& l, const glm::vec3& r);
}

class VoxelsComponent : public Component
{
private:
	map<ivec3,Voxel*> voxels;
	vector<Voxel*> allVoxels;

	bool needUpdateMesh;
	ivec3 min;
	ivec3 max;

public:
	VoxelsComponent();
	~VoxelsComponent();
	void setVoxel(Voxel* voxel);
	Voxel* getVoxelAt(ivec3 vec);
	Voxel* getVoxelAt(int x, int y, int z);
	void splitVoxelAt(ivec3 vec);
	ivec3 getMin()const;
	ivec3 getMax()const;
	void update();
	void updateMesh();
	void reduceVoxel();

private:
	void reduceMeshUp(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices);
	void reduceMeshDown(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices);
	void reduceMeshLeft(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices);
	void reduceMeshRight(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices);
	void reduceMeshFront(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices);
	void reduceMeshBack(vector<float>& vertices, vector<float>& normals, vector<float>& colors, vector<int>& indices);
};