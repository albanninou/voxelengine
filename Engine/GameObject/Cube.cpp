#include "Cube.h"

Cube::Cube(vec3 position, vec3 rotation, vec3 size) : GameObject(position, rotation), color(1, 1, 1)
{
	voxelsComponent = new VoxelsComponent();
	addComponent(voxelsComponent);
	voxelsComponent->setVoxel(new Voxel(ivec3(0,0,0), ivec3(size.x, size.y, size.z),1, color));
}

Cube::Cube(vec3 position, vec3 rotation, vec3 size, vec3 color) : GameObject(position, rotation), color(color)
{
	voxelsComponent = new VoxelsComponent();
	addComponent(voxelsComponent);
	voxelsComponent->setVoxel(new Voxel(ivec3(0, 0, 0), ivec3(size.x, size.y, size.z), 1, color));
}

Cube::Cube(vec3 position, vec3 rotation, vec3 size, vec3 color, vec3 scale) :GameObject(position, rotation, scale), color(color)
{
	voxelsComponent = new VoxelsComponent();
	addComponent(voxelsComponent);

	for (int x = 0; x < size.x; x++) {
		for (int y = 0; y < size.y; y++) {
			for (int z = 0; z < size.z; z++) {
				voxelsComponent->setVoxel(new Voxel(ivec3(x, y, z), ivec3(1,1,1), 1, color));
			}
		}
	}

}

Cube::~Cube()
{
}