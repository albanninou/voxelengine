#pragma once
#include "GameObject.h"
#include "Components\Voxel\VoxelsComponent.h"

class Cube : public GameObject
{
public:
	VoxelsComponent* voxelsComponent;
	vec3 color;

	Cube(vec3 position, vec3 rotation, vec3 size);
	Cube(vec3 position, vec3 rotation, vec3 size, vec3 color);
	Cube(vec3 position, vec3 rotation, vec3 size, vec3 color, vec3 scale);
	~Cube();

private:

};