#pragma once
#include "GameObject.h"
#include "Components\Shader.h"


GameObject::GameObject() :components(), position(0, 0, 0), rotation(0, 0, 0), scale(1, 1, 1), center(0, 0, 0)
{
	this->needUpdateTransformation = true;
}

GameObject::GameObject(vec3 & position, vec3& rotation) : GameObject()
{
	this->position = position;
	this->rotation = rotation;
}

GameObject::GameObject(vec3 & position, vec3 & rotation, vec3 & scale) :GameObject(position, rotation)
{
	this->scale = scale;
	this->center = vec3(scale.x / 2.0, scale.y / 2.0, scale.z / 2.0);
}

GameObject::GameObject(vec3 & position, vec3 & rotation, float scale) : GameObject(position, rotation)
{
	this->scale = vec3(scale, scale, scale);
	this->center = vec3(scale / 2.0, scale / 2.0, scale / 2.0);
}

GameObject::~GameObject()
{
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		if (it->second->getGameObject() == this) {
			delete (it->second);
		}
	}
}

void GameObject::render()
{
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		if (it->first == typeid(Shader).name()) {
			continue;
		}
		it->second->render();
	}
}

void GameObject::update()
{
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		it->second->update();
	}
}

void GameObject::updateVoxel()
{
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		it->second->updateVoxel();
	}
}

vec3 GameObject::getPosition() const
{
	return position;
}

vec3 GameObject::getRotation() const
{
	return rotation;
}

vec3 GameObject::getScale() const
{
	return scale;
}

void GameObject::setRotation(vec3 vec)
{
	vec3 deltaRot = vec3(vec - rotation);
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		it->second->addRotation(deltaRot);
	}
	rotation = vec;
	needUpdateTransformation = true;
}

void GameObject::addRotation(vec3 vec)
{
	if (vec.x == 0 && vec.y == 0 && vec.z == 0) {
		return;
	}
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		it->second->addRotation(vec);
	}
	rotation += vec;
	needUpdateTransformation = true;
}

void GameObject::setPosition(vec3 vec)
{
	vec3 deltaRot = vec3(vec - position);
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		it->second->addPosition(deltaRot);
	}
	position = vec;
	needUpdateTransformation = true;
}

void GameObject::setCenter(vec3 vec)
{
	this->center = vec3(vec.x*scale.x,vec.y*scale.y,vec.z*scale.z);
}

void GameObject::addPosition(vec3 vec)
{
	if (vec.x == 0 && vec.y == 0 && vec.z == 0) {
		return;
	}
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		it->second->addPosition(vec);
	}
	position += vec;
	needUpdateTransformation = true;
}

void GameObject::addComponent(Component* component)
{
	string test = typeid(*component).name();
	component->setGameObject(this);
	components[test] = component;
}

mat4* GameObject::getTransformationMatrix()
{
	if (needUpdateTransformation) {
		transformationMatrix = createTransformationMatrix(position, center, rotation, scale);
		needUpdateTransformation = false;
	}
	return &transformationMatrix;
}