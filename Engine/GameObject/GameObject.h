#pragma once
#include "Components\Mesh.h"
#include "..\Tool\Tools.h"
#include "..\Tool\Maths.h"
#include "Components\Component.h"

class GameObject
{
private:
	map<string, Component*> components;
	vec3 position;
	vec3 center;
	vec3 rotation;
	vec3 scale;
	mat4 transformationMatrix;
	bool needUpdateTransformation;

public:
	float shineDamper = 1;
	float reflectivity = 0;

	GameObject();
	GameObject(vec3& position, vec3& rotation);
	GameObject(vec3& position, vec3& rotation, vec3& scale);
	GameObject(vec3& position, vec3& rotation, float scale);
	~GameObject();

	void render();
	void update();
	void updateVoxel();
	vec3 getPosition() const;
	vec3 getRotation() const;
	vec3 getScale() const;
	void setPosition(vec3 vec);
	void setCenter(vec3 vec);
	void addPosition(vec3 vec);
	void setRotation(vec3 vec);
	void addRotation(vec3 vec);
	template <typename Type> Type* getComponent();
	void addComponent(Component* component);
	mat4* getTransformationMatrix();

private:

};

template<typename Type>
Type* GameObject::getComponent()
{
	map<string, Component*>::iterator it;
	for (it = components.begin(); it != components.end(); it++)
	{
		if (it->first == typeid(Type).name()) {
			return dynamic_cast<Type*>(components[it->first]);
		}
	}
	return nullptr;
}