#pragma once
#include "GameObjectRender.h"

GameObjectRender::GameObjectRender()
{
}

GameObjectRender::~GameObjectRender()
{
	map<Shader*, vector<GameObject*>>::iterator it;
	for (it = gameObjects.begin(); it != gameObjects.end(); it++)
	{
		if (it->first != nullptr) {
			delete it->first;
		}
	}
}

void GameObjectRender::render()
{
	Camera* cam = nullptr;
	if (!cameras.empty()) {
		cam = cameras.back();
	}
	if (cam == nullptr) {
		return;
	}
	for (auto& kv : gameObjects)
	{
		Shader* shader = kv.first;
		shader->start();
		shader->loadViewMatrix(*cam->getViewMatrix());
		shader->loadLigths(lights);

		for (auto gameObject : kv.second) {
			mat4* transformation = gameObject->getTransformationMatrix();
			shader->loadTransformationMatrix(*transformation);
			shader->loadShineVariable(gameObject->shineDamper, gameObject->reflectivity);
			gameObject->render();
		}
		shader->stop();
	}
}

void GameObjectRender::update()
{
}

void GameObjectRender::addGameObject(GameObject* gameObject)
{
	Shader* shader = gameObject->getComponent<Shader>();
	if (shader != nullptr) {
		if (gameObjects.find(shader) == gameObjects.end()) {
			gameObjects[shader] = vector<GameObject*>();
		}
		gameObjects[shader].push_back(gameObject);
	}
	Camera* camera = gameObject->getComponent<Camera>();
	if (camera != nullptr) {
		cout << "you set one camera" << endl;
		cameras.push_back(camera);
	}
	Light* light = gameObject->getComponent<Light>();
	if (light != nullptr) {
		cout << "you set one light" << endl;
		lights.push_back(light);
	}
}

void GameObjectRender::updateProjectionMatrix(mat4 &projectionMatrix)
{
	map<Shader*, vector<GameObject*>>::iterator it;
	for (it = gameObjects.begin(); it != gameObjects.end(); it++)
	{
		Shader& shader = (*it->first);
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}
}