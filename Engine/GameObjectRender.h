#pragma once
#include <vector>
#include "GameObject\Components\Shader.h"
#include "GameObject\GameObject.h"
#include "Tool\Maths.h"
#include "GameObject\Components\Camera.h"
#include "GameObject\Components\Light.h"

class GameObjectRender
{
private:
	map<Shader*, vector<GameObject*>> gameObjects;
	vector<Camera*> cameras;
	vector<Light*> lights;

public:
	GameObjectRender();
	~GameObjectRender();
	void render();
	void update();
	void addGameObject(GameObject* gameObject);
	void updateProjectionMatrix(mat4 &projectionMatrix);
private:

};