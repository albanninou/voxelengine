#pragma once
#include "Maths.h"

mat4 createTransformationMatrix(vec3& translation, vec3& center, vec3& rotation, vec3& scale) {
	glm::mat4 matrix = translate(glm::mat4(1.0), translation);

	matrix = translate(matrix, center);
	matrix = rotate(matrix, radians(rotation.x), vec3(1, 0, 0));
	matrix = rotate(matrix, radians(rotation.y), vec3(0, 1, 0));
	matrix = rotate(matrix, radians(rotation.z), vec3(0, 0, 1));
	matrix = translate(matrix, -center);
	matrix = glm::scale(matrix, scale);
	return matrix;
}

mat4 createViewMatrix(vec3& translation, vec3& rot)
{
	glm::mat4 matrix = rotate(glm::mat4(1.0), radians(rot.x), vec3(1, 0, 0));
	matrix = rotate(matrix, radians(rot.y), vec3(0, 1, 0));
	matrix = rotate(matrix, radians(rot.z), vec3(0, 0, 1));
	matrix = translate(matrix, -translation);
	return matrix;
}