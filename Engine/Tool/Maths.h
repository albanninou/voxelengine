#pragma once
#include "Tools.h"

mat4 createTransformationMatrix(vec3& translation, vec3& center, vec3& rotation, vec3& scale);
mat4 createViewMatrix(vec3& translation, vec3& rot);