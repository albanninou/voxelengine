#pragma once
#include <gl/glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include <array>
#include <map>

#include <glm/glm.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <glm/trigonometric.hpp>
// Include GLM extensions
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;
using namespace std;

#define COLOR_RED vec3(0.8f,0,0)
#define COLOR_GREEN vec3(0,0.8f,0)
#define COLOR_BLUE vec3(0,0,0.8f)
#define COLOR_WHITE vec3(1,1,1)