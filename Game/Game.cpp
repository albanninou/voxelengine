#include "Game.h"

Game::Game(Engine* engine)
{
	this->engine = engine;
	init();
}

Game::~Game()
{
}

void Game::init()
{
	shader = new Shader("Data/Shaders/vertexShader.txt", "Data/Shaders/fragmentShader.txt");
	engine->initShader(shader);

	Cube* base = new Cube(vec3(0, -1, 0), vec3(0, 0, 0), vec3(16, 1, 16), COLOR_RED);
	base->addComponent(shader);
	base->reflectivity = 1;
	base->shineDamper = 100;
	engine->addGameObject(base);

	player = new Cube(vec3(2, 7, 0), vec3(0, 180, 0), vec3(1, 1, 1), COLOR_WHITE);
	player->addComponent(new Camera());
	player->addComponent(new  PlayerController());
	player->addComponent(new BoundingBox());
	engine->addGameObject(player);

	Cube* light = new Cube(vec3(0, 4, 0), vec3(0, 0, 0), vec3(1, 1, 1), COLOR_WHITE);
	light->addComponent(shader);
	light->addComponent(new Light(vec3(1, 1, 1), vec3(1, 0.01f, 0.002f)));
	engine->addGameObject(light);

	Cube* light2 = new Cube(vec3(15, 4, 0), vec3(0, 0, 0), vec3(1, 1, 1), COLOR_WHITE);
	light2->addComponent(shader);
	light2->addComponent(new Light(vec3(1, 1, 1), vec3(1, 0.01f, 0.002f)));
	engine->addGameObject(light2);

	Cube* light3 = new Cube(vec3(0, 4, 15), vec3(0, 0, 0), vec3(1, 1, 1), COLOR_WHITE);
	light3->addComponent(shader);
	light3->addComponent(new Light(vec3(1, 1, 1), vec3(1, 0.01f, 0.002f)));
	engine->addGameObject(light3);

	Cube* light4 = new Cube(vec3(15, 4, 15), vec3(0, 0, 0), vec3(1, 1, 1), COLOR_WHITE);
	light4->addComponent(shader);
	light4->addComponent(new Light(vec3(1, 1, 1), vec3(1, 0.01f, 0.002f)));
	engine->addGameObject(light4);

	Cube* testCube = new Cube(vec3(7, 4, 7), vec3(0, 0, 0), vec3(50, 10, 10), COLOR_BLUE, vec3(0.5, 0.5, 0.5));
	testCube->addComponent(shader);
	testCube->reflectivity = 1;
	testCube->shineDamper = 100;
	testCube->addComponent(new Test());
	engine->addGameObject(testCube);

	GameObject* test = new GameObject(vec3(0, 0, 0), vec3(0, 0, 0));
	test->addComponent(shader);
	test->reflectivity = 1;
	test->shineDamper = 100;
	test->addComponent(new BoundingBox());
	//test->addComponent(new Test());
	VoxelsComponent* voxelComponent = new VoxelsComponent();
	test->addComponent(voxelComponent);
	voxelComponent->setVoxel(new Voxel(vec3(0, 0, 0), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(0, 1, 0), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(1, 0, 0), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(2, 0, 0), vec3(1, 1, 1), 2, vec3(0, 1, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(2, 0, 1), vec3(1, 1, 1), 2, vec3(0, 1, 0)));

	voxelComponent->setVoxel(new Voxel(vec3(4, 0, 0), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(5, 0, 0), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(6, 0, 0), vec3(1, 1, 1), 1, vec3(1, 0, 0)));

	voxelComponent->setVoxel(new Voxel(vec3(0, 0, 1), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(1, 0, 1), vec3(1, 1, 1), 1, vec3(1, 0, 0)));

	voxelComponent->setVoxel(new Voxel(vec3(0, 0, 2), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(1, 0, 2), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(0, 1, 2), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(1, 1, 2), vec3(1, 1, 1), 1, vec3(1, 0, 0)));

	voxelComponent->setVoxel(new Voxel(vec3(0, 0, 3), vec3(1, 1, 1), 1, vec3(1, 0, 0)));

	voxelComponent->setVoxel(new Voxel(vec3(4, 0, 4), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(4, 0, 5), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(5, 0, 4), vec3(1, 1, 1), 1, vec3(1, 0, 0)));
	voxelComponent->setVoxel(new Voxel(vec3(5, 0, 5), vec3(1, 1, 1), 1, vec3(1, 0, 0)));


	engine->addGameObject(test);
}