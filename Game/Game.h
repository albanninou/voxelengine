#pragma once
#include "..\Engine\Engine.h"
#include "Test.h"
#include "PlayerController.h"

class Game
{
public:
	Engine* engine;
	GameObject* player;
	Shader* shader;

	Game(Engine* engine);
	~Game();

	void init();
private:

};