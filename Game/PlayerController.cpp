#include "PlayerController.h"

PlayerController::PlayerController()
{
}

PlayerController::~PlayerController()
{
}

void PlayerController::update()
{
	if (Engine::mouseDelta.y != 0 || Engine::mouseDelta.x != 0) {
		gameObject->addRotation(vec3(Engine::mouseDelta.y*sensibility, Engine::mouseDelta.x*sensibility, 0));
	}

	float xDir = 0;
	float yDir = 0;
	float zDir = 0;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
		zDir = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		zDir = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
		xDir = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		xDir = speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		yDir = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
		yDir = -speed;
	}
	vec3 rot = gameObject->getRotation();
	float xa = xDir * cos(radians(rot.y)) - zDir * sin(radians(rot.y));
	float ya = yDir;
	float za = zDir * cos(radians(rot.y)) + xDir * sin(radians(rot.y));
	gameObject->addPosition(vec3(xa, ya, za));
}