#include "Test.h"

Test::Test()
{
}

Test::~Test()
{
}

void Test::update()
{
	//gameObject->setRotation(gameObject->getRotation()+vec3(0.05f, 0.1f, 0.2f));
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		gameObject->addPosition(vec3(0.01, 0, 0));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
		gameObject->addPosition(vec3(-0.01, 0, 0));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
		gameObject->addRotation(vec3(0, 5, 0));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::V)) {
		gameObject->addRotation(vec3(0, -5, 0));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::B)) {
		gameObject->getComponent<VoxelsComponent>()->setVoxel(new Voxel(ivec3(20, 3, 4), ivec3(1, 1, 1), 3, vec3(0, 1, 0)));
	}

	
}

void Test::render()
{
}